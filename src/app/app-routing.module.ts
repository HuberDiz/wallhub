import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeedComponent } from './component/feed/feed.component';
import {LoginComponent} from "./component/login/login.component"
import { RegisterComponent } from './component/register/register.component';
import { UserAreaComponent } from './component/user-area/user-area.component';
import { UserConfigComponent } from './component/user-config/user-config.component';
import { OtherUserAreaComponent } from './component/other-user-area/other-user-area.component';

import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'feed', component: FeedComponent , canActivate:[AuthGuard]},
  {path: 'userArea', component: UserAreaComponent, canActivate:[AuthGuard]},
  {path: 'userConfig', component: UserConfigComponent, canActivate:[AuthGuard]},
  {path: 'otherUserArea', component: OtherUserAreaComponent, canActivate:[AuthGuard]},
  
  ];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
