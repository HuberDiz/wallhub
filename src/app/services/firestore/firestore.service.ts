import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(private firestore : AngularFirestore) { }

  //Crea un nuevo usuario
  public createUser(data: { nickname:string, firstName:string, lastName:string, phone:string, email:string, birthDate:Date}){
    return this.firestore.collection('users').add(data);
  }

  //Obtiene un usuario
  public getDocument(documentId:string, collection:string){
    return this.firestore.collection(collection).doc(documentId).snapshotChanges();
  }

  //Obtiene todos los usuarios
  public getDocuments(collection:string){
    return this.firestore.collection(collection).snapshotChanges();
  }

  //Actualiza un usuario
  public updateDocument(documentId:string,data:any,collection:string){
    return this.firestore.collection(collection).doc(documentId).set(data);
  }

  //Obtener Post de un usuario
  public getPostOwner(userId: string){
    return this.firestore.collection('users').doc(userId).collection('post').snapshotChanges();
  }

  //Obtener Comment de un Post
  public getCommentPost(userId: string, postId: string){
    return this.firestore.collection('users').doc(userId).collection('post').doc(postId).collection('comment').snapshotChanges();
  }

}
