import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { environment } from 'src/environments/environment';
import { FeedComponent } from './component/feed/feed.component';
import { MenuComponent } from './component/menu/menu.component';
import { PostComponent } from './component/post/post.component';
import { DialogOverviewExampleDialog, PostListComponent } from './component/post-list/post-list.component';
import { UserAreaComponent } from './component/user-area/user-area.component';
import { ProfileComponent } from './component/profile/profile.component';
import { FriendsComponent } from './component/friends/friends.component';
import { UserConfigComponent } from './component/user-config/user-config.component';
import { AuthGuard } from './guards/auth.guard';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatButton, MatButtonModule } from '@angular/material/button'
import { LastPostService } from './last-post.service';
import { OtherUserAreaComponent } from './component/other-user-area/other-user-area.component';
import {MatCardModule} from '@angular/material/card'; 


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    FeedComponent,
    MenuComponent,
    PostComponent,
    PostListComponent,
    UserAreaComponent,
    ProfileComponent,
    FriendsComponent,
    UserConfigComponent,
    PostDetailComponent,
    DialogOverviewExampleDialog,
    OtherUserAreaComponent
    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFireAuthModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    HttpClientModule,
    MatCardModule
    

    
  ],
  providers: [
    LoginComponent,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    }
  ],
  entryComponents: [
    DialogOverviewExampleDialog
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
