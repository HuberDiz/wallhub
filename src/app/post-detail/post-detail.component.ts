import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { DialogOverviewExampleDialog } from '../component/post-list/post-list.component';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PostDetailComponent implements OnInit {

  @Input() postInfo
  user:any = [];
  userLastPost:any;

  constructor(public dialog:MatDialog,private afAuth: AngularFireAuth,
    private router: Router,private db: AngularFirestore) { }

  ngOnInit(): void {
    if (this.user.nickname == undefined){
      this.user.nickname = "Anonymus"
    }
    firebase.default.firestore().collection('users').doc(this.postInfo.post_owner).get().then(userSnap => {
      if (userSnap.exists){
        this.user = userSnap.data()
      }
    })
  }

  openDialog(post): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '80vw',
      data: { post: post},
    });

    dialogRef.afterClosed().subscribe((result) => {

    });
  }

  navigateUserArea(){
   this.afAuth.onAuthStateChanged(auth => {
     if(this.postInfo.post_owner == auth.uid){
      this.router.navigate(['/userArea'])
     }else{
       this.db.collection('users').doc(this.postInfo.post_owner).ref.get().then(doc => {

        localStorage.removeItem('userSearched')

        localStorage.setItem('userSearched',JSON.stringify(doc.data()))
 
       this.router.navigate(['/otherUserArea'])
       })
      
     }
   })
  }



}
