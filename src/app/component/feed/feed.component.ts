import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase'

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FeedComponent implements OnInit {

  user:any;

  constructor(public router :Router) { }

  ngOnInit(): void {
    firebase.default.auth().onAuthStateChanged((user) => {
      firebase.default.firestore().collection('users').doc(user.uid).get().then(res=>{
        localStorage.setItem('user',JSON.stringify(res.data()));
      })
    })
  }
}
