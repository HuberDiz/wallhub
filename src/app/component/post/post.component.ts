import { AngularFirestore } from '@angular/fire/firestore';
import { environment } from './../../../environments/environment.prod';
import { AngularFireModule } from '@angular/fire';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { getLocaleDateFormat } from '@angular/common';
import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFireStorage,
  AngularFireStorageReference,
  AngularFireUploadTask,
} from '@angular/fire/storage';
import { merge } from 'rxjs';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
  encapsulation:  ViewEncapsulation.None,
})
export class PostComponent implements OnInit {
  data: any;
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  src: any;
  imgevent: any;
  media: boolean;

  user: any

  public newPostForm = new FormGroup({
    message: new FormControl(
      '',
      Validators.compose([Validators.required, Validators.minLength(4)])
    ),
    image: new FormControl(''),
  });

  constructor(private db: AngularFirestore, private afAuth: AngularFireAuth, private afStorage: AngularFireStorage) {

    this.media = false;
  }

  async sendPost(form) {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    var d = new Date();

    var imageUrl = null;

    // Si contiene foto
    if (this.media) {
      firebase.default.auth().onAuthStateChanged(async (user) => {
        document.getElementById('messageInput').style.height = '65px';

        // Se sube la foto al cloud storage
        await (await this.ref.put(this.imgevent[0])).ref
          .getDownloadURL()
          .then(function (downloadURL) {
            imageUrl = downloadURL;
          });

        var contentType;

        // await this.ref.getMetadata().forEach((e)=>{
        //   contentType = e.contentType.substring(0,5);

        // })
        contentType = this.imgevent[0].type.substring(0, 5);

        //Se define data y se le asigna la url de la imagen en el campo image
        this.data = {
          message: form.message,
          image: imageUrl,
          post_owner: user.uid,
          media: true,
          reactions: [],
          publishDate: dd + '/' + mm + '/' + yyyy,
          media_content_type: contentType,
          post_id:''

        }
        localStorage.setItem('lastPost', JSON.stringify(this.data));
        var postRef = this.db
          .collection('users')
          .doc(user.uid)
          .collection('post')
          .add(this.data)
          .then((doc) => {
            this.db
              .collection('users')
              .doc(user.uid)
              .collection('post')
              .doc(doc.id)
              .set(
                {
                  post_id: doc.id,
                },
                { merge: true }
              ).then(e => {
                location.reload()
              });
          });
      });
    } else {
      firebase.default.auth().onAuthStateChanged(async (user) => {
        document.getElementById('messageInput').style.height = '65px';

        //Se define data y se le asigna la url de la imagen en el campo image
        this.data = {
          message: form.message,
          image: null,
          post_owner: user.uid,
          media: false,
          reactions: [],
          publishDate: dd + '/' + mm + '/' + yyyy,
          post_id:''

        }
        var postRef = this.db.collection('users').doc(user.uid).collection('post').add(this.data).then((doc) => {
          this.db.collection('users').doc(user.uid).collection('post').doc(doc.id).set({
            post_id:doc.id
          },{ merge: true }).then(e => {
            location.reload()
          });
        })
      })
    }

    //Añadimos el post a local storage para mostrar una vez enviado.
    await localStorage.setItem('lastPost', JSON.stringify(this.data));
    await localStorage.setItem('lastPost', JSON.stringify(this.data));

    this.newPostForm.setValue({
      message: '',
      image: '',
    });
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    this.media = true;
  }

  ngOnInit(): void {

    this.user = JSON.parse(localStorage.getItem('user'))

    this.afAuth.onAuthStateChanged(user => {
      this.db.collection('users').get()
    })

  }

  textAreaAdjust() {
    let element = document.getElementById('messageInput');
    element.style.height = '1px';
    element.style.height = element.scrollHeight + 'px';
  }

  upload(event) {
    const id = Math.random().toString(36).substring(2);
    this.ref = this.afStorage.ref(id);
    this.imgevent = event.target.files;
    this.media = true;
  }
}
