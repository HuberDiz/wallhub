import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {

  private info: any

  private friends:any
  public nameFriends:any[]

  private friendsDelete:any[]
  private friendsSave:any[]


  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth,
    private firestoreService: FirestoreService,
    private router: Router
  ) { }

  ngOnInit(): void {
    firebase.default.auth().onAuthStateChanged((user) => {
      this.firestoreService.getDocument(user.uid,'users').subscribe((res) => {
        this.info = res.payload.data()
        this.friends = this.info.friends
        this.nameFriends = []
        for(let entry of this.friends){
          if(entry != user.uid){
            let datos = this.db.collection('users').doc(entry).ref
            datos.get().then((ed) => {
              this.info = ed.data()
              let middle = {
                uid: entry,
                nickname: this.info.nickname,
                avatar: this.info.avatar
              }
              this.nameFriends.push(middle)
            })
          }
        }
      })
    });
  }

  private deleteFriends(userDelete){
    this.friendsDelete = []
    this.friendsSave = []
    firebase.default.auth().onAuthStateChanged((user) => {
      let datos = this.db.collection('users').doc(user.uid).ref
      datos.get().then((ed) => {
        this.info = ed.data()
        this.friendsDelete = this.info.friends
        this.friendsDelete.forEach(element => {
          if(element == userDelete){
          }else{
            this.friendsSave.push(element)
          }
        });
        datos.set({
          friends: this.friendsSave,
        }, {merge: true});
      })
    })
  }


  navigateUserArea(friendUid){
    this.afAuth.onAuthStateChanged(auth => {
      if(friendUid == auth.uid){
        this.router.navigate(['/userArea'])
       }else{
        this.db.collection('users').doc(friendUid).ref.get().then(doc => {
  
          localStorage.removeItem('userSearched')
  
          localStorage.setItem('userSearched',JSON.stringify(doc.data()))
   
         this.router.navigate(['/otherUserArea'])})
         
       }
    })
}

}