import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, docChanges } from '@angular/fire/firestore';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public _data:any;
  public authUser:any ={
    avatar: ""
  };
  public findUser = new FormGroup({
    name: new FormControl(''),
  });
  userSearchResult: unknown;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore,
    ) {
      this.findUser.setValue({
        name: '',
      });
     }

  ngOnInit():void {
    this.afAuth.onAuthStateChanged(auth => {
      this.db.collection('users').doc(auth.uid).ref.get().then(doc => {
        this.authUser = doc.data()
      })
    })
  }

  SignOut() {
    localStorage.clear();
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      sessionStorage.removeItem("currentUser")
      this.router.navigate(['']);
    })
  }

  searchUser(_value){
    localStorage.removeItem('userSearched')
    
    const find_user = _value.name;
    let q1 = this.db.collection('users').ref.where("nickname","==",find_user).get().then((e) => {
      e.forEach((doc) => {
        this.userSearchResult = doc.data();
        localStorage.setItem('userSearched',JSON.stringify(doc.data()));
        this.afAuth.onAuthStateChanged( e => {
          this._data = doc.data();

          if(this.router.url == '/otherUserArea'){
            location.reload()
          }
          if(this._data.friends[0] != e.uid){
            this.router.navigate(['/otherUserArea'])
          }
          
        })
      })
    })
  }


}
