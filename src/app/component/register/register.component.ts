import { Component, NgZone, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthServiceService } from 'src/app/services/authService/auth-service.service';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';
import {Router} from '@angular/router';
import * as firebase from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public documentId = null;
  public currentStatus = 1;
  public newUserForm = new FormGroup({
    nickname: new FormControl('', Validators.required),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    phone: new FormControl(''),
    email: new FormControl('', Validators.required),
    birthDate: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    confirmPassword: new FormControl('', Validators.required),
    id: new FormControl('')
  });
  constructor(
    private router: Router,
    private firestoreService: FirestoreService,
    public authService: AuthServiceService,
    public ngZone : NgZone) {
    this.newUserForm.setValue({
      id: '',
      nickname: '',
      firstName: '',
      lastName: '',
      phone: '',
      email: '',
      password: '',
      confirmPassword: '',
      birthDate: ''
    });
   }

  navigateToLogin() {
    this.router.navigate(['login']);
  }

  ngOnInit(): void {
  }

  public newUser(form, documentId = this.documentId){
    let numberPass = false;
    let capitalLetter = false;
    let lowerCase = false;

    const dataForm = {
      nickname: form.nickname,
      firstName: form.firstName,
      lastName: form.lastName,
      email: form.email,
      phone: form.phone,
      birthDate: form.birthDate,
    };

    if(dataForm.nickname.length <= 16){
      if(dataForm.phone.length <= 15){
        if (form.password === form.confirmPassword) {

        let userPassword = form.password
        if(userPassword.length <= 8){
          alert('tiene que tener 8 caracteres')
        }else{
         for(var i = 0; i< userPassword.length; i++){
          if(userPassword.charAt(i) == 0 || userPassword.charAt(i) == 1 || userPassword.charAt(i) == 2 ||
          userPassword.charAt(i) == 3 || userPassword.charAt(i) == 4 || userPassword.charAt(i) == 5 ||
          userPassword.charAt(i) == 6 || userPassword.charAt(i) == 7 || userPassword.charAt(i) == 8 ||
          userPassword.charAt(i) == 9){
            numberPass = true
          }else if(userPassword.charAt(i) == 'a' || userPassword.charAt(i) == 'b' || userPassword.charAt(i) == 'c' ||
          userPassword.charAt(i) == 'd' || userPassword.charAt(i) == 'e' || userPassword.charAt(i) == 'f' ||
          userPassword.charAt(i) == 'g' || userPassword.charAt(i) == 'h' || userPassword.charAt(i) == 'i' ||
          userPassword.charAt(i) == 'j' || userPassword.charAt(i) == 'k' || userPassword.charAt(i) == 'l' ||
          userPassword.charAt(i) == 'm' || userPassword.charAt(i) == 'n' || userPassword.charAt(i) == 'o' ||
          userPassword.charAt(i) == 'p' || userPassword.charAt(i) == 'q' || userPassword.charAt(i) == 'r' ||
          userPassword.charAt(i) == 's' || userPassword.charAt(i) == 't' || userPassword.charAt(i) == 'u' ||
          userPassword.charAt(i) == 'v' || userPassword.charAt(i) == 'w' || userPassword.charAt(i) == 'x' ||
          userPassword.charAt(i) == 'y' || userPassword.charAt(i) == 'z' || userPassword.charAt(i) == 'ñ'){
            lowerCase = true
          }else if(userPassword.charAt(i) == 'A' || userPassword.charAt(i) == 'B' || userPassword.charAt(i) == 'C' ||
          userPassword.charAt(i) == 'D' || userPassword.charAt(i) == 'E' || userPassword.charAt(i) == 'F' ||
          userPassword.charAt(i) == 'G' || userPassword.charAt(i) == 'H' || userPassword.charAt(i) == 'I' ||
          userPassword.charAt(i) == 'J' || userPassword.charAt(i) == 'K' || userPassword.charAt(i) == 'L' ||
          userPassword.charAt(i) == 'M' || userPassword.charAt(i) == 'N' || userPassword.charAt(i) == 'O' ||
          userPassword.charAt(i) == 'P' || userPassword.charAt(i) == 'Q' || userPassword.charAt(i) == 'R' ||
          userPassword.charAt(i) == 'S' || userPassword.charAt(i) == 'T' || userPassword.charAt(i) == 'U' ||
          userPassword.charAt(i) == 'V' || userPassword.charAt(i) == 'W' || userPassword.charAt(i) == 'X' ||
          userPassword.charAt(i) == 'Y' || userPassword.charAt(i) == 'Z' || userPassword.charAt(i) == 'Ñ'){
            capitalLetter = true
          }
        }
       }
       }else{
        alert('La contraseña no coincide')
       }
      }else{
        console.log('phone tiene mas de 15')
      }
    }else{
      console.log('tiene mas de 16')
    }



    if(numberPass == true && lowerCase == true && capitalLetter == true){
      this.tryRegister(form.email, form.password, dataForm);
    }else{
      alert('La contraseña tiene que contener: 8 caracteres, 1 letra en mayuscula, 1 letra en minuscula y un numero')
      this.setValuesInWhitePassword()
    }
  }

  tryRegister(email, password, dataForm) {
    firebase.default.auth().createUserWithEmailAndPassword(email, password).then(async (res) => {
      res.user.updateProfile({
        displayName: dataForm.nickname,
      })

      res.user.sendEmailVerification().then(function() {
        alert('SE LE HA ENVIADO UN EMAIL A SU EMAIL PARA VERIFICAR')
      }).catch(function(error) {
        // An error happened.
      });

      let friends = [res.user.uid]

      let data = {
        friends: friends,
        avatar : await this.getRandomAvatar(),
        nickname: dataForm.nickname,
        firstName: dataForm.firstName,
        lastName: dataForm.lastName,
        email: dataForm.email,
        phone: dataForm.phone,
        birthDate: dataForm.birthDate,
      }

      this.firestoreService.updateDocument(res.user.uid,data,'users').then(() => {
        this.setValuesInWhiteAll()
      } , (error) => {

      });
      firebase.default.auth().signOut()
      this.router.navigate([''])
    }, err => {
      alert(err);
    });
  }

  async getRandomAvatar(){
    let array = []
    await firebase.default.firestore().collection('avatars').get().then(res => {
      res.docs.forEach(data => {
        array.push(data.data())
      })
    })
    let random = Math.floor(Math.random()*array.length)
    return (array[random].url);
  }

  public setValuesInWhiteAll(){
    this.newUserForm.setValue({
      id: '',
      nickname: '',
      firstName: '',
      lastName: '',
      phone: '',
      email: '',
      password: '',
      confirmPassword: '',
      birthDate: ''
    });
  }

  public setValuesInWhitePassword(){
    this.newUserForm.setValue({
      password: '',
      confirmPassword: '',
    });
  }

}
