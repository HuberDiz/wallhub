import { Component, NgZone, OnInit } from '@angular/core';
import { AuthServiceService } from 'src/app/services/authService/auth-service.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase'



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userData:any;

  public loginForm = new FormGroup({
    user: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  
  constructor(
    private authService: AuthServiceService,
    private router: Router,
    private ngZone: NgZone,
    private afAuth: AngularFireAuth) { }

  ngOnInit(): void {
    this.afAuth.onAuthStateChanged(user => {
      if (user) {
        this.ngZone.run(() => {
          this.router.navigate(['feed'])
        }
        )
      } else {
        this.ngZone.run(()=>{
          this.router.navigate([''])
        })
      }
    })
  }

  update(): void{
    location.reload();
  }
  login(form): void {
    firebase.default.auth().signInWithEmailAndPassword(form.user,form.password).then(()=>{
      this.afAuth.onAuthStateChanged(auth => {
        console.log("currentuser",auth.email)
        sessionStorage.setItem("currentUser",JSON.stringify(auth.uid))
      })
      this.router.navigate(['/feed'])
    }, error => {
      alert(error)
    }
    )
  }
  get isLoggedIn():boolean {
    const user = JSON.parse(localStorage.getItem('user'))
    return (user !== null) ? true : false;
  }
}
