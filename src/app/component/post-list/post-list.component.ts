
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireStorageModule, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { Component, OnInit, Input, Inject, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';

import { listLazyRoutes } from '@angular/compiler/src/aot/lazy_routes';
import { from, Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { environment } from '../../../environments/environment';
import * as firebase from 'firebase';
import { TestBed } from '@angular/core/testing';
import { ROUTER_CONFIGURATION } from '@angular/router';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { LastPostService } from 'src/app/last-post.service';
import { Subscription } from 'rxjs';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';

export interface DialogData {
  posts: any[];
  post: any;
  userInfo: any;
  items: any;
  userLastPost: any;
  listaReactions: String[];
  listaReactionsLength: any;
  pressed: boolean;
}

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnInit {
  clickEventsubscription: Subscription;
  public posts: any[];
  public userInfo: any;
  public items: any;
  public userLastPost: any;
  public listaReactions: String[];
  public listaReactionsLength: any = 0;
  public pressed: boolean;
  public lastpost: any;
  public sortedList: any[];
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;

  constructor(private db: AngularFirestore, public afAuth: AngularFireAuth, private afStorage: AngularFireStorage, public dialog: MatDialog, private postButton: LastPostService) {




  }
  count: number = 0;
  incrementCount() {
    this.count++;
  }


  openDialog(post): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '80vw',
      data: { post: post },
    });

    dialogRef.afterClosed().subscribe((result) => {

    });
  }

  async ngOnInit(): Promise<void> {


    // coje los posts del usuario autentificado y los posts de los amigo

    await firebase.default.auth().onAuthStateChanged(async (user) => {
      var userRef = this.db.collection('users').doc(user.uid).ref;
      await userRef.get().then(async (doc) => {
        this.items = doc.data();
        this.items.friends.forEach((friend) => {
          this.posts = [];
          this.db
            .collection('users')
            .doc(friend)
            .collection('post')
            .get()
            .forEach((res) => {
              res.forEach((res) => {
                this.posts.push(res.data());
              });
              this.posts.forEach((post, index) => {
                if (post.reactions.includes(user.uid)) {
                  setTimeout(() => {
                    document.getElementById("brick-btn" + index).style.fill = "#7e5e9d"
                  }, 1)
                }
              })
            });
        });
      });
    });

    // 
    await firebase.default.auth().onAuthStateChanged(async (user) => {
      var userRef = this.db.collection('users').doc(user.uid).ref;
      await userRef.get().then(async (doc) => {
        this.items = doc.data();
        await this.items.friends.forEach((friend) => {
          this.sortedList = [];
          this.db
            .collection('users')
            .doc(friend)
            .collection('post')
            .get()
            .forEach((res) => {
              res.forEach((res) => {
                this.sortedList.push(res.data());
              });
              this.posts.forEach((post, index) => {
                if (post.reactions.includes(user.uid)) {
                  setTimeout(() => {
                    document.getElementById("brick-btn" + index).style.fill = "#7e5e9d"
                  }, 1)
                }
              })
            })
            this.compareDatesCustom(this.sortedList)
            ;
        })
        
      })
    })


  }

  reaction(_post, i) {
    firebase.default.auth().onAuthStateChanged((user) => {
      let postRef = this.db
        .collection('users')
        .doc(_post.post_owner)
        .collection('post')
        .doc(_post.post_id).ref;
      postRef.get().then((doc) => {
        this.listaReactions = doc.data().reactions;
        this.listaReactionsLength = this.listaReactions.length;

        if (!this.listaReactions.includes(user.uid)) {
          document.getElementById("brick-btn" + i).style.fill = "#7e5e9d"
          this.listaReactions.push(user.uid);
          postRef.set(
            {
              reactions: this.listaReactions,
            },
            { merge: true }
          );
        } else {
          let index = this.listaReactions.indexOf(user.uid);
          document.getElementById("brick-btn" + i).style.fill = "#c2c2c2"
          this.listaReactions.splice(index, 1);
          postRef.set(
            {
              reactions: this.listaReactions,
            },
            { merge: true }
          );
        }
        this.posts.forEach((post) => {
          if (_post.post_id == post.post_id) {
            if (post.reactions.includes(user.uid)) {
              let index = post.reactions.indexOf(user.uid);
              post.reactions.splice(index, 1);
            } else {
              post.reactions.push(user.uid);
            }
          }
        });
      });
    });
  }

  //sort

compareDates(d1, d2){
  var parts =d1.split('/');
   d1 = Number(parts[2] + parts[1] + parts[0]);
  parts = d2.split('/');
   d2 = Number(parts[2] + parts[1] + parts[0]);
  return d1 <= d2;
  }

  compareDatesCustom(lista){
    lista.sort((obj1,obj2) => {
      var string1 = new String(obj1.publishDate)
      var string2 = new String(obj2.publishDate)
      console.log(string1)
      console.log(string2)

    })

  
  }

}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
  styleUrls: ['dialog.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DialogOverviewExampleDialog implements OnInit {
  posts: any[];
  userInfo: any;
  items: any;
  userLastPost: any;
  listaReactions: String[];
  listaReactionsLength: any;
  pressed: boolean;
  comments: any[];
  commentList: any[];
  fullCommentList: any[];
  lastpost: any;
  user: any;
  postOwner: any = {
    avatar: "",
    nickname: "",
  };
  infoUser: any;
  infoReturnNickname: any;
  infoSearchUser: { name_owner: any; avatar_owner: any; data: any; };
  listaLikesComments: any[];
  _commentReactions: any[]
  commentReactionsLength: any[];
  contador: number;
  xdlist: any[];




  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private db: AngularFirestore, public afAuth: AngularFireAuth, private afStorage: AngularFireStorage, private http: HttpClient, private postButton: LastPostService) {
    this.lastpost = 1;
  }


  textAreaAdjust() {
    let element = document.getElementById('messageInput')
    element.style.height = "1px"
    element.style.height = element.scrollHeight + "px"
  }
  async ngOnInit(): Promise<void> {
    console.log("dis data", this.data)
    firebase.default.firestore().collection('users').doc(this.data.post.post_owner).get().then(res => {
      this.postOwner = res.data();
    })

    // this.getCommentLikes()
    this.lastpost = 1;
    this.postButton.getPost()
    this.commentList = []
    this.listaLikesComments = []
    this._commentReactions = []
    this.commentReactionsLength = []
    this.contador = 0;

    // Http
    // this.http.get('http://wallhub-test-api.epizy.com/public/random-image').forEach(res => {

    // })

    //Get post list
    await firebase.default.auth().onAuthStateChanged(async (user) => {
      var userRef = this.db.collection('users').doc(user.uid).ref;
      await userRef.get().then(async (doc) => {
        this.items = doc.data();
        this.items.friends.forEach((friend) => {
          this.posts = [];
          this.db
            .collection('users')
            .doc(friend)
            .collection('post')
            .get()
            .forEach((res) => {
              res.forEach((res) => {
                this.posts.push(res.data());
                this.posts.forEach((post) => {

                  if (this.data.post.reactions.includes(user.uid)) {
                    setTimeout(() => {
                      document.getElementById("brick-btn").style.fill = "#7e5e9d"
                    }, 1)
                  }
                })
              });
            });
        });
      });
    });


    this.getComments()
    console.log("list", this.commentList)
    // this.delay(100)
    // this.commentList.forEach(e => {
    //   console.log("bicos", e)
    //   this.afAuth.onAuthStateChanged(auth => {
    //     if (e.data.reactions.includes(auth.uid)) {
    //       document.getElementById("brick-btn-comment" + this.contador).style.fill = "#7e5e9d"
    //     }
    //   })

    // })
  }
  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  

  getComments() {
    //Get Comments
    firebase.default.auth().onAuthStateChanged(async (user) => {
      let commentListRef = this.db.collection('users').doc(this.data.post.post_owner).collection('post').doc(this.data.post.post_id).collection('comment').ref;
      await commentListRef.get().then((doc) => {

        doc.forEach(doc => {
          this.db.collection('users').doc(doc.data().comment_owner).ref.get().then((as) => {
            this.infoUser = as.data()
            //
            this.infoReturnNickname = this.infoUser.nickname

            this.infoSearchUser = {
              name_owner: this.infoReturnNickname,
              avatar_owner: this.infoUser.avatar,
              data: doc.data(),
            }

            this.commentList.push(this.infoSearchUser)
          })
        })
      })
    })
  }



  onNoClick(): void {
    this.dialogRef.close();
  }

  reaction(_post) {

    firebase.default.auth().onAuthStateChanged((user) => {
      let postRef = this.db
        .collection('users')
        .doc(_post.post_owner)
        .collection('post')
        .doc(_post.post_id).ref;
      postRef.get().then((doc) => {
        this.listaReactions = doc.data().reactions;
        this.listaReactionsLength = this.listaReactions.length;

        if (!this.listaReactions.includes(user.uid)) {
          this.listaReactions.push(user.uid);
          postRef.set(
            {
              reactions: this.listaReactions,
            },
            { merge: true }
          );
          document.getElementById("brick-btn").style.fill = "#7e5e9d"

        } else {
          let index = this.listaReactions.indexOf(user.uid);
          this.listaReactions.splice(index, 1);
          postRef.set(
            {
              reactions: this.listaReactions,
            },
            { merge: true }
          );
          document.getElementById("brick-btn").style.fill = "#c2c2c2"
        }
        this.posts.forEach((post) => {
          if (_post.post_id == post.post_id) {
            if (_post.reactions.includes(user.uid)) {
              let index = post.reactions.indexOf(user.uid);
              _post.reactions.splice(index, 1);
            } else {
              _post.reactions.push(user.uid);
            }
          }
        });
      });
    });
  }
  likeDislikeComment(comment, index) {
    this.afAuth.onAuthStateChanged(auth => {
      this.db.collection('users').doc(this.data.post.post_owner).collection('post').doc(this.data.post.post_id).collection('comment').doc(comment.data.comment_id)
        .ref.get().then(commentData => {

          //Si ya esta like
          if (this.commentList[index].data.reactions.includes(auth.uid)) {

            //CAmbios en array local

            var i = this.commentList[index].data.reactions.indexOf(auth.uid);
            this.commentList[index].data.reactions.splice(i, 1);

            //cambios en firestore

            this.db.collection('users').doc(this.data.post.post_owner).collection('post').doc(this.data.post.post_id).collection('comment').doc(comment.data.comment_id)
              .ref.set({
                reactions: this.commentList[index].data.reactions
              }, { merge: true })

          } else {
            //CAmbios en array local
            this.commentList[index].data.reactions.push(auth.uid)
            //cambios en firestore
            this.db.collection('users').doc(this.data.post.post_owner).collection('post').doc(this.data.post.post_id).collection('comment').doc(comment.data.comment_id)
              .ref.set({
                reactions: this.commentList[index].data.reactions
              }, { merge: true })
          }

        })
    })

  }

  // getCommentLikes(){
  //   this.afAuth.onAuthStateChanged(auth => {
  //     this.db.collection('users').doc(this.data.post.post_owner).collection('post').doc(this.data.post.post_id).collection('comment').ref
  //     .get().then(likesComments => {
  //      likesComments.forEach(element => {
  //        this.listaLikesComments.push(element.data())
  //      });
  //     })
  //   }).then((e) => {
  //     console.log(this.listaLikesComments)
  //   })
  // }
  public newPostForm = new FormGroup({
    message: new FormControl(
      '',
      Validators.compose([Validators.required, Validators.minLength(4)])
    ),
  });

  sendComment(comment, _post) {
    console.log("xdd")
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    var d = new Date();


      var currentUsr = JSON.parse(sessionStorage.getItem("currentUser"))
      if(firebase.default.auth().currentUser.uid == currentUsr){
        console.log("estoy auth")

        let commentListRef = this.db
        .collection('users')
        .doc(_post.post_owner)
        .collection('post')
        .doc(_post.post_id)
        .collection('comment')
        .add({
          comment_owner: firebase.default.auth().currentUser.uid,
          message: comment.message,
          reactions: [],
          release_date: dd + '/' + mm + '/' + yyyy,
        }).then(e => {
          let commentListRef = this.db
            .collection('users')
            .doc(_post.post_owner)
            .collection('post')
            .doc(_post.post_id)
            .collection('comment')
            .doc(e.id)
            .set({
              comment_id: e.id
            }, { merge: true })
        })

      }
      


    this.newPostForm.setValue({
      message: '',
    });
  }
  @ViewChild('videoPlayer') videoplayer: ElementRef;

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }

  getUserData(uid) {
    firebase.default.firestore().collection('users').doc(uid).get().then(res => {

      return res.data();
    })
  }



}
