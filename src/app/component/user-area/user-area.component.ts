import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-user-area',
  templateUrl: './user-area.component.html',
  styleUrls: ['./user-area.component.scss']
})
export class UserAreaComponent implements OnInit {

  profile:boolean= true;
  config:boolean= false;
  friends:boolean= false;
  userInfo:any;
  authorized:boolean;

  constructor(public afAuth: AngularFireAuth) {
    this.userInfo = JSON.parse(localStorage.getItem('userSearched'))
   }

  ngOnInit(): void {
    this.afAuth.onAuthStateChanged((e) => {
     
      if(e.uid == this.userInfo.friends[0]){
          this.authorized = true;

      }
      else{
        this.authorized = false;
      }
    })
  }

}
