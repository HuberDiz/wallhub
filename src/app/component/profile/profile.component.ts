import { Component, Inject, NgZone, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as firebase from 'firebase'
import { FirestoreService } from 'src/app/services/firestore/firestore.service';
import { DialogData, DialogOverviewExampleDialog } from '../post-list/post-list.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit {



  public user:any;
  public posts:any[]
  listaReactions: any[];
  listaReactionsLength: any;

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth,
    private firestoreService: FirestoreService,
    public dialog:MatDialog,
  ) {
    this.posts = []
   }

  openDialog(post): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '80vw',
      data: { post: post},
    });

    dialogRef.afterClosed().subscribe((result) => {

    });
  }

  ngOnInit(): void {


    firebase.default.auth().onAuthStateChanged(user=>{
          //get user
      this.db.collection('users').doc(user.uid).ref.get().then( doc => {
        this.user = doc.data()
      })
    })


    //get posts
    this.afAuth.onAuthStateChanged(auth => {
       //get post list
       this.db.collection('users').doc(auth.uid).collection('post').ref.get().then(lista => {
        lista.forEach(docPost => {
          console.log(docPost.data())
            this.posts.push(docPost.data())
        })
        this.posts.forEach((post, index) => {
          if (post.reactions.includes(auth.uid)) {
            setTimeout(() => {
              document.getElementById("brick-btn" + index).style.fill = "#7e5e9d"
            }, 1)
          }
        })
      });
    })
  }


  reaction(_post,i) {
    this.afAuth.onAuthStateChanged((user) => {
      let postRef = this.db
        .collection('users')
        .doc(_post.post_owner)
        .collection('post')
        .doc(_post.post_id).ref;
      postRef.get().then((doc) => {
        this.listaReactions = doc.data().reactions;
        this.listaReactionsLength = this.listaReactions.length;

        if (!this.listaReactions.includes(user.uid)) {
          document.getElementById("brick-btn"+i).style.fill = "#7e5e9d"
          this.listaReactions.push(user.uid);
          postRef.set(
            {
              reactions: this.listaReactions,
            },
            { merge: true }
          );
        } else {
          let index = this.listaReactions.indexOf(user.uid);
          document.getElementById("brick-btn"+i).style.fill = "#c2c2c2"
          this.listaReactions.splice(index, 1);
          postRef.set(
            {
              reactions: this.listaReactions,
            },
            { merge: true }
          );
        }
        this.posts.forEach((post) => {
          if (_post.post_id == post.post_id) {
            if (post.reactions.includes(user.uid)) {
              let index = post.reactions.indexOf(user.uid);
              post.reactions.splice(index, 1);
            } else {
              post.reactions.push(user.uid);
            }
          }
        });
      });
    });
  }


}
