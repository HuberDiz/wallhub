import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';


import { MatCardModule } from '@angular/material/card';
import { DialogOverviewExampleDialog } from '../post-list/post-list.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-other-user-area',
  templateUrl: './other-user-area.component.html',
  styleUrls: ['./other-user-area.component.scss'],
})
export class OtherUserAreaComponent implements OnInit {
  public searchPerson: any;
  public myFriendList: any[];
  public friend: boolean;
  public posts:any[];
  currentuser:any;
  public newFriendList:any[];
  listaReactions: any[];
  listaReactionsLength: any;




  constructor(private afAuth: AngularFireAuth,
    private router: Router,
    private db: AngularFirestore,public dialog:MatDialog) {
    this.searchPerson = JSON.parse(localStorage.getItem('userSearched'))
    console.log("constructor", this.searchPerson)
    this.friend = false;
  }

  async ngOnInit(): Promise<void> {
    this.posts = []

      this.currentuser = JSON.parse(localStorage.getItem('user'))

    

    //GET FRIEND LIST
    await this.afAuth.onAuthStateChanged(auth => {
      this.db.collection('users').doc(auth.uid).ref.get().then(doc => {
        var auxData;
        auxData = doc.data()
        this.myFriendList = auxData.friends;
        console.log("my  auth uid is ", auth.uid)
      }).then(e => {
        //CHECK IF FRIEND
        this.myFriendList.forEach(friend => {
          console.log("hola")
          if (friend == this.searchPerson.friends[0]) {
            this.friend = true;
          }
        })
      })
    })
    
    //get User Post POST
    this.afAuth.onAuthStateChanged(auth => {
      this.db.collection('users').doc(this.searchPerson.friends[0]).collection('post').ref.get().then(docFetch => {
        docFetch.forEach(doc => {
          this.posts.push(doc.data())
        })
        this.posts.forEach((post, index) => {
          if (post.reactions.includes(auth.uid)) {
            setTimeout(() => {
              document.getElementById("brick-btn" + index).style.fill = "#7e5e9d"
            }, 1)
          }
        })
      })
    })

  }




//FUNCION PARA EL BOTON DE FOLLOW/UNFOLLOW
  followUnfollow(){
    if(this.friend == false){
      this.afAuth.onAuthStateChanged((auth) => {
        this.myFriendList.push(this.searchPerson.friends[0])
        this.db.collection('users').doc(auth.uid).set({
          friends: this.myFriendList
      }, { merge: true }).then((e )=>{
        this.friend = true;
        console.log("friend añadido")
        
      });
      })
    }
    if(this.friend == true){
      this.afAuth.onAuthStateChanged((auth) => {
        var index = this.myFriendList.indexOf(this.searchPerson.friends[0])
        this.myFriendList.splice(index,1)
        this.db.collection('users').doc(auth.uid).set({
          friends: this.myFriendList
      }, { merge: true }).then((e )=>{
        this.friend = false;
        console.log("friend quitado")
        
      });
      })
    }
  }
  

  //Dialog
  openDialog(post): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '80vw',
      data: { post: post},
    });

    dialogRef.afterClosed().subscribe((result) => {

    });
  }

  reaction(_post,i) {
    this.afAuth.onAuthStateChanged((user) => {
      let postRef = this.db
        .collection('users')
        .doc(_post.post_owner)
        .collection('post')
        .doc(_post.post_id).ref;
      postRef.get().then((doc) => {
        this.listaReactions = doc.data().reactions;
        this.listaReactionsLength = this.listaReactions.length;

        if (!this.listaReactions.includes(user.uid)) {
          document.getElementById("brick-btn"+i).style.fill = "#7e5e9d"
          this.listaReactions.push(user.uid);
          postRef.set(
            {
              reactions: this.listaReactions,
            },
            { merge: true }
          );
        } else {
          let index = this.listaReactions.indexOf(user.uid);
          document.getElementById("brick-btn"+i).style.fill = "#c2c2c2"
          this.listaReactions.splice(index, 1);
          postRef.set(
            {
              reactions: this.listaReactions,
            },
            { merge: true }
          );
        }
        this.posts.forEach((post) => {
          if (_post.post_id == post.post_id) {
            if (post.reactions.includes(user.uid)) {
              let index = post.reactions.indexOf(user.uid);
              post.reactions.splice(index, 1);
            } else {
              post.reactions.push(user.uid);
            }
          }
        });
      });
    });
  }
}