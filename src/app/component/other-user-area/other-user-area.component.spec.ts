import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherUserAreaComponent } from './other-user-area.component';

describe('OtherUserAreaComponent', () => {
  let component: OtherUserAreaComponent;
  let fixture: ComponentFixture<OtherUserAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherUserAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherUserAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
