import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  AngularFireStorage,
  AngularFireStorageReference,
  AngularFireUploadTask,
} from '@angular/fire/storage';

@Component({
  selector: 'app-user-config',
  templateUrl: './user-config.component.html',
  styleUrls: ['./user-config.component.scss'],
})
export class UserConfigComponent implements OnInit {
  public documentId = null;
  col1 = false;
  col2 = false;
  col3 = false;
  col4 = false;
  col5 = false;
  ref: AngularFireStorageReference;
  imgevent: any;
  media: boolean;

  public updateUser = new FormGroup({
    nickname: new FormControl(''),
    biography: new FormControl(''),
    phone: new FormControl(''),
    password: new FormControl(''),
    confirmPassword: new FormControl(''),
  });



  constructor(
    private db: AngularFirestore,
    private router: Router,
    private afAuth: AngularFireAuth,
    private afStorage: AngularFireStorage,
  ) {
    this.updateUser.setValue({
      nickname: '',
      biography:'',
      phone:'',
      password:'',
      confirmPassword:'',
    });
    this.media = false;
   }

  ngOnInit(): void {
  }

  public collectingData(form){
    const dataUserUpdate = {
      nickname: form.nickname,
      biography: form.biography,
      phone: form.phone,
      password: form.password,
      confirmPassword: form.confirmPassword,
    };



    this.setValuesInWhite();
    if(form.password != "" && form.confirmPassword != ""){
      if(form.password === form.confirmPassword){
        this.changePassword(dataUserUpdate);
      }else{
        alert('Las constraseñas no coinciden')
      }
    }else if (form.nickname != "" || form.biography != "" || form.phone != ""){
      this.findUser(dataUserUpdate);
    }
  }


  findUser(dataUserUpdate){
    if(dataUserUpdate.nickname.length <= 16){
      if(dataUserUpdate.phone.length <= 15){
        if(dataUserUpdate.biography.length <= 300){
            firebase.default.auth().onAuthStateChanged((user) => {
              var userFind = this.db.collection('users').doc(user.uid).ref
              if(dataUserUpdate.phone != ""){
                userFind.set({
                  phone: dataUserUpdate.phone,
                }, {merge: true});
              }
              if(dataUserUpdate.biography != ""){
                userFind.set({
                biography: dataUserUpdate.biography,
                }, {merge: true});
              }
              if(dataUserUpdate.nickname != ""){
                userFind.set({
                nickname: dataUserUpdate.nickname,
                }, {merge: true});
              }
            });
        }else{
          alert('EL BIOGRAPHY NO PUEDE TENER MÁS DE 300 CARACTERES')
        }
      }else{
        alert('EL PHONE NO PUEDE TENER MAS DE 15 CARACTERES')
      }
    }else{
      alert('EL NICKNAME NO PUEDE TENER MAS DE 16 CARACTERES')
    }
  }

  changePassword(dataUserUpdate){
    let numberPass = false;
    let capitalLetter = false;
    let lowerCase = false;

    let userPassword = dataUserUpdate.password

    if(userPassword.length <= 7){
      alert('tiene que tener 8 caracteres')
    }else{

      for(var i = 0; i< userPassword.length; i++){
        if(userPassword.charAt(i) == 0 || userPassword.charAt(i) == 1 || userPassword.charAt(i) == 2 ||
        userPassword.charAt(i) == 3 || userPassword.charAt(i) == 4 || userPassword.charAt(i) == 5 ||
        userPassword.charAt(i) == 6 || userPassword.charAt(i) == 7 || userPassword.charAt(i) == 8 ||
        userPassword.charAt(i) == 9){
          numberPass = true
        }else if(userPassword.charAt(i) == 'a' || userPassword.charAt(i) == 'b' || userPassword.charAt(i) == 'c' ||
        userPassword.charAt(i) == 'd' || userPassword.charAt(i) == 'e' || userPassword.charAt(i) == 'f' ||
        userPassword.charAt(i) == 'g' || userPassword.charAt(i) == 'h' || userPassword.charAt(i) == 'i' ||
        userPassword.charAt(i) == 'j' || userPassword.charAt(i) == 'k' || userPassword.charAt(i) == 'l' ||
        userPassword.charAt(i) == 'm' || userPassword.charAt(i) == 'n' || userPassword.charAt(i) == 'o' ||
        userPassword.charAt(i) == 'p' || userPassword.charAt(i) == 'q' || userPassword.charAt(i) == 'r' ||
        userPassword.charAt(i) == 's' || userPassword.charAt(i) == 't' || userPassword.charAt(i) == 'u' ||
        userPassword.charAt(i) == 'v' || userPassword.charAt(i) == 'w' || userPassword.charAt(i) == 'x' ||
        userPassword.charAt(i) == 'y' || userPassword.charAt(i) == 'z' || userPassword.charAt(i) == 'ñ'){
          lowerCase = true
        }else if(userPassword.charAt(i) == 'A' || userPassword.charAt(i) == 'B' || userPassword.charAt(i) == 'C' ||
        userPassword.charAt(i) == 'D' || userPassword.charAt(i) == 'E' || userPassword.charAt(i) == 'F' ||
        userPassword.charAt(i) == 'G' || userPassword.charAt(i) == 'H' || userPassword.charAt(i) == 'I' ||
        userPassword.charAt(i) == 'J' || userPassword.charAt(i) == 'K' || userPassword.charAt(i) == 'L' ||
        userPassword.charAt(i) == 'M' || userPassword.charAt(i) == 'N' || userPassword.charAt(i) == 'O' ||
        userPassword.charAt(i) == 'P' || userPassword.charAt(i) == 'Q' || userPassword.charAt(i) == 'R' ||
        userPassword.charAt(i) == 'S' || userPassword.charAt(i) == 'T' || userPassword.charAt(i) == 'U' ||
        userPassword.charAt(i) == 'V' || userPassword.charAt(i) == 'W' || userPassword.charAt(i) == 'X' ||
        userPassword.charAt(i) == 'Y' || userPassword.charAt(i) == 'Z' || userPassword.charAt(i) == 'Ñ'){
          capitalLetter = true
        }
      }
    }
    if(numberPass == true && lowerCase == true && capitalLetter == true){
      this.afAuth.onAuthStateChanged(user => {
        user.updatePassword(dataUserUpdate.password)
        alert("Contrase actualizada User Config")
      }, (error) => {
        alert('Problemas al actualizar la contraseña prueba de nuevo');
      })
    }else{
      alert('La contraseña tiene que contener: 8 caracteres, 1 letra en mayuscula, 1 letra en minuscula y un numero')
    }
  }

  setValuesInWhite(){
    this.updateUser.setValue({
      nickname: '',
      biography:'',
      phone:'',
      password:'',
      confirmPassword:''
    });
  }

  uncollapse(i,action){
    if(action){
      document.getElementById("content"+i).style.display = "flex"
      setTimeout(()=>{
        document.getElementById("content"+i).style.height = "17vh"
      },50)
    }else{
      setTimeout(()=>{
        document.getElementById("content"+i).style.display = "none"
      },1000)
      document.getElementById("content"+i).style.height = "0vh"

    }
  }
  send(){
    var imageUrl = null;
    if (this.media) {
      firebase.default.auth().onAuthStateChanged(async (user) => {

          // Se sube la foto al cloud storage
        await (await this.ref.put(this.imgevent[0])).ref
          .getDownloadURL()
          .then(function (downloadURL) {
            imageUrl = downloadURL;
          });

        var contentType;

        contentType = this.imgevent[0].type.substring(0, 5);

        var userFind = this.db.collection('users').doc(user.uid).ref
        if(imageUrl != ""){
          userFind.set({
          avatar: imageUrl,
          }, {merge: true});
        }
      })
    }
  }

  upload(event) {
    const id = Math.random().toString(36).substring(2);
    this.ref = this.afStorage.ref(id);
    this.imgevent = event.target.files;
    this.media = true;
    this.send()
  }

}
